<h1>Detector de Huevos Fisurados</h1>
por Miguel Angel Gomez <mikiangel10@hotmail.com>

<h3>Resumen:</h3>
En este  Proyecto intentaré escribir una función en Python3 para detectar a partir de una foto de un huevo, si este presenta o no alguna fisura en su cáscara.
Como dispositivo de captura de la foto pienso usar una cámara de 5Mpx usada desde una Raspberry Pi 3 B+, corriendo Raspbian-stretch.
En este mismo dispositivo correría la función que detecta la fisura en el huevo.
Según he estimado necesitaría tres fotos del contorno del huevo para analizar toda la superficie.
Pensando en aplicar este dispositivo detector en una seleccionadora de 3600 huevos/hora, tendría que analizar 3 imágenes por segundo.
Debido a esto, tengo un límite de tiempo de 330 ms para tomar la imágen y procesarla.
La configuración del dispositivo, los paquetes necesarios y las rutinas para obtener las imágenes estarán dentro del proyecto, aunque el objetivo de este sea la función que realiza el análisis.
La Función recibiría una imágen como argumento y el retorno será True en caso de hallar la fisura y False si no.

<h3>Estado del Arte:</h3>
La detección automática de huevos rotos llevo a muchas personas a trabajar en el tema desde hace muchos años. Ya en 1962 se trabajo en el tema con un dispositivo que utilizaba la variacion de la elasticidad de la estructura en entre un huevo sano y uno fisurado. En este enlace  https://patents.google.com/patent/US3067605 se puede ver la patente de dicho dispositivo.
Obviamente la precisión de este método es muy baja, y mediante técnicas digitales se pudo mejorar este sistema aprovechando la misma propiedad. Actualmente es el método más utilizado y cuenta con un pequeño martillo que golpea suavemente cada huevo y un receptor de sonido "escucha" la frecuencia de resonancia del golpe, pudiendo detectar los fisurados con una precisión del 95% aproximadamente. En este enlace http://int.moba.net/page/es/Grading/Moba-Grader-Options/Detection-Systems/Crack-detection se puede ver un  sistema comercial de los varios que están en el mercado usando este método. Acerca de este método también hay varios estudios realizados, entre los que se pueden señalar https://www.sciencedirect.com/science/article/pii/S0021863400905420 , o https://www.sciencedirect.com/science/article/pii/S0260877414005329.
El principal problema de este método es la calibración apropiada de los martillos y sensores, la presencia de piezas móviles y el desgaste que se produce, lo que requiere ajustes periódicos.
Para la detección visual de huevos rotos, lo que más se usa es la visión humana, siendo más o menos efectiva dependiendo de la capacidad y entrenamiento del operador. Este tipo de proceso es único utilizado en granjas de tamaño medio-chico, y aún es utilizado en plantas de procesamiento de gran tamaño. Generalmente este proceso es el cuello de botella de la línea de producción.
Para acelerar el proceso se busca la visión artificial. Después de 2010 se han hecho varios trabajos investigativos en este sentido, obteniendo resultados cada vez más alentadores, con distintos niveles de comlejidad. Por ejemplo, el departamento de agricultura de EEUU investigó que aplicando visión artificial en huevos en un ambiente de presión negativa, la detección es casi total y sin falsos positivos. Acá el enlace https://www.vision-systems.com/articles/print/volume-14/issue-12/departments/technology-trends/food-amp-beverage-vision-system-speeds-egg-crack-detection.html . Otro trabajo de investigación muy completo y con menor complejidad es este <https://www.researchgate.net/publication/304168490_DETECTION_OF_CRACK_IN_EGG_WITH_IMAGE_PROCESSING_TECHNIQUE>, de un docente de la universidad SRM de India. También el trabajo conjunto de investigadores  de la Universidad Politecnica de Madrid y la Universita degli Studi di Milano es muy orientador sobre las tecnologías a utilizar para lograr el objetivo buscado. Este es el enlace <http://oa.upm.es/9612/1/Articolo_Pubblicato_Definitivo.pdf>. La principal diferencia entre estos trabajos y lo que yo pretendo son las plataformas utilizadas, tanto la cámara como la plataforma de desarrollo y el lenguaje.(Ambos utilizan MATLAB). Cabe señalar que estos estudios también incluyen la detección de suciedad en el huevo, cosa que es muy necesaria, pero que estaría fuera del alcance de mi proyecto.  

<h3>Licencias:</h3>
La documentación será licenciada bajo CreativeCommons Atribucion - Share Alike version 4.0.
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

<h3>Objetivo General:</h3>
El objetivo final de este proyecto es incorporar la detección de huevos fisurados en una seleccionadora automática de huevos. La función se deberá ejecutar en tiempo real hasta tres veces por segundo, y  luego mediante el GPIO del Raspberry Pi se enviará una señal al controlador principal de la seleccionadora para que esta descarte el huevo fisurado de la linea de selección. 
En el comienzo del proyecto se hará incapié en la función que analice la imagen que estará almacenada en disco en busca de fisuras. Estimo que este es el paso más difícl del proyecto. Luego se incluirá el algoritmo y se estructurará un programa para que la imagen en tiempo real pase por la función, sin tener que almacenarla en disco, para ahorrar tiempo.
Otro punto a determinar es la forma de obtener las imágenes del huevo desde 3 puntos distintos, separados 120º cada uno, para obtener una visión completa de toda la superficie de la cáscara. En este punto se evalúa la posibilidad de girar el huevo en su lugar mediante rodillos, y que un Detector le tome las tres fotos, o que el huevo gire a medida que avanza, y que haya 3 detectores, en tres posiciones concecutivas, cada uno que analice la imagen de una parte de cada huevo. Para decidir esto es importante saber cuanto demorará la ejecución de la función con la imagen en tiempo real.

<h3>Objetivos Específicos:</h3>
<ul>
<li><h4>Objetos Físicos:</h4>
Lo primero antes de empezar con el código se necestian algunos objetos para la ambientación física del hardware. Algunas serán piezas que se podrán imprimir.
Se fabricará una pieza que llamaré "Exhibidor" la cual emulará a los rolos en los que se transporta el huevo en la seleccionadora, ya que entrarán en la imagen  y tendrán que ser distinguidos y descartados dentro del algoritmo de vision artificial. Esta pieza está en el repositorio hecha en Freecad, dentro de la carpeta "piezas" y se llama "exhibidor.fcstd". Se necesitan imprimir dos piezas iguales y se arma el exhibidor uniéndolas enfrentadas.
La otra pieza impresa será el soporte de la cámara, que tendrá dos movimientos para poder regular el ángulo de enfoque, la distancia y la altura de la cámara, parámetros que iremos evaluando para obtener las imágenes que mejor nos convengan a medida que vayamos avanzando con el código. Esta pieza consta de tres partes, que están en la carpeta "piezas/soporte-camara" y son "pie-soporte.fcstd", "brazo-medio.fcstd" y "brazo-soporte.fcstd". 
Esta planeado subir las piezas en formato STL también
El resto del Hardware que se utilizará para el proyecto es el Raspberry Pi 3 Model B+, alimentada con fuente original Canakit de 5V-2,5A.
La cámara es una OV5647, con las siguientes especificaciones: CCD de 1/4", Apertura: 1,8º, diámetro Focal: 3,6mm ajustable, resolucion máxima: 1080p. Con iluminación auxiliar infraroja con dos leds de 3W. 
</li>
</ul>
